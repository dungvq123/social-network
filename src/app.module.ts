import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';

@Module({
  imports: [
    TasksModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'dung',
      password: '1',
      database: 'task-manager',
      autoLoadEntities: true,
      synchronize: true,
      // logging: true,
    }),
    AuthModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
