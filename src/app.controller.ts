import { Controller, Get } from '@nestjs/common';

@Controller('/')
export class AppController {
  @Get()
  helloworld() {
    return 'hello world';
  }
}
